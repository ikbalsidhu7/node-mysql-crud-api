# node-crud-api-mysql 

 CRUD API Example - Node.js + MySQL  
 
Install all required npm packages by running npm install or npm i from the command line in the project root folder.

Update the database credentials in /config.json to connect to your MySQL server instance.

Start the API by running npm start
you should see the message Server listening on port 4000.

You can check the API with Postman.